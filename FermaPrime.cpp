//
// Created by Яна on 01.11.2022.
//

#include "PRIME.h"

const int m=999999;

bool FERMA::primecheck(LongInt a) {
const int T = 2;
    srand(time(0));
    LongInt pminus1 = a-LongInt(1);
    int i;
    for (i = 0; i < T; i++) {
        LongInt a(rand()%m + 2);
        LongInt x = ModPow(a, pminus1, a);
        if (x == LongInt(1)) {
            continue;
        }
        return false;
    }
    return true;
}

