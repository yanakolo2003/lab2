//
// Created by Яна on 01.11.2022.
//


#include "PRIME.h"

const int m=999999;
const int T=2;

bool Solovay_Strassen::primecheck (LongInt a) {
    srand(time(0));
    LongInt pminus1 = a-LongInt(1);
    LongInt n = pminus1/LongInt(2);
    int i;
    for (i = 0; i < T; i++) {
        LongInt b(rand() %m+ 2);
        LongInt x = ModPow(b, n, a);
        if (x == LongInt(1) || x == LongInt(0) || x == pminus1) {
            continue;
        }
        return false;
    }
    return true;
}
