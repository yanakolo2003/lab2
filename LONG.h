//
// Created by Яна on 23.10.2022.
//

#include <string>
#include <cmath>
#include <vector>
using namespace std;

#ifndef LAB2_LONG_H
class LongInt;
class Multiplication{
public:
    virtual LongInt multiply(LongInt a,LongInt b)=0;
};

class LongInt;
class PrimeCheck{
public:
    virtual bool primecheck(LongInt a)= 0;
};

class LongInt{
private:
    int Base=(int)log10(BASE);
    static Multiplication *multiplication;
    static PrimeCheck *Check;
public:
    static void setMultMetod(Multiplication *mult);
    static void setPrimeMethod(PrimeCheck *prc);
    static const int BASE=10000;
    vector<int> digits;
    LongInt(string a);
    LongInt(int a);
    int len();
    int Revers(int a);
    void shift(int a);
    LongInt split (int a);
    static void equalize(LongInt &x, LongInt &y);
    string ConvToStr();
    LongInt operator+(LongInt other);
    LongInt operator*(LongInt other);
    LongInt operator-(LongInt other);
    LongInt operator/(LongInt other);
    LongInt operator%(LongInt other);
    LongInt operator*(int other);
    bool Is_Prime();
    void RemoveZeros();


    friend class KaraMult;
    friend class FERMA;
    friend class Solovay_Strassen;

};

ostream& operator <<(ostream& os,  LongInt x);
bool operator<(LongInt _1,LongInt _2);
bool operator== (LongInt a, LongInt b);
bool operator<=(LongInt x, LongInt y);

LongInt ModPow(LongInt a, LongInt p, LongInt mod);
string Conclusion(bool a);

#define LAB2_LONG_H

#endif //LAB2_LONG_H