//
// Created by Яна on 23.10.2022.
//

#include "LONG.h"
#ifndef LAB2_KARA_H

class KaraMult: public Multiplication{
public:
    LongInt multiply(LongInt a,LongInt b);
};

class ToomCook3: public Multiplication{
public:
    LongInt multiply(LongInt a,LongInt b);
};

#define LAB2_KARA_H
#endif //LAB2_KARA_H
