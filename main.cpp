#include "LONG.h"
#include <iostream>
#include "MULT.h"
#include "PRIME.h"

int main(){

    string a("4675");
    string b("25565676567");
    LongInt::setMultMetod(new KaraMult());
    auto x=LongInt(a);
    auto y=LongInt(b);
    auto res1=x*y;
    cout<<"Karatsuba method, multiplication x*y= "<<res1<<";"<<endl;


    LongInt::setPrimeMethod(new FERMA());
    auto p1=LongInt("123456");
    auto res3=p1.Is_Prime();
    cout<<"Ferma method, number "<<p1<<Conclusion(res3)<<";"<<endl;

    LongInt::setPrimeMethod(new Solovay_Strassen());
    auto p2=LongInt("9973");
    auto res4=p2.Is_Prime();
    cout<<"Solovay Strassen method, number "<<p2<<Conclusion(res4)<<";"<<endl;

    return 0;
}