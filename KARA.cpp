//
// Created by Яна on 23.10.2022.
//

#include <iostream>
#include "MULT.h"

LongInt KaraMult::multiply (LongInt a,LongInt b){
    a.equalize(a,b);
    if((a.len()!=1)&&(b.len()!=1)){

        LongInt a1=a.split(1);
        LongInt a2=a.split(2);
        //a.equalize(a1,a2);
        LongInt b1=b.split(1);
        LongInt b2=b.split(2);


        LongInt x=multiply(a1,b1);
        LongInt y=multiply(a1+a2,b1+b2);
        LongInt z=multiply(a2,b2);
        LongInt res(0);
        auto w=x;
        auto s=(y-x-z);
        w.shift(a.len()-a.len()/2+a.len()-a.len()/2);
        s.shift(a.len()-a.len()/2);
        res=w+s+z;
        return res;
    }
    else{
        LongInt res(0);
        res.digits = vector<int>(2, 0);
        res.digits[0]=(a.digits[0]*b.digits[0])%a.BASE;
        res.digits[1]=(a.digits[0]*b.digits[0])/b.BASE;
        return res;
    }
}
