//
// Created by Яна on 23.10.2022.
//

#include "LONG.h"

#include <string>
#include <vector>
#include <algorithm>
#include <iostream>


using namespace std;

Multiplication *LongInt::multiplication=nullptr;

PrimeCheck *LongInt::Check=nullptr;

int Pow(int x,int y) {
    int res = 1;
    while (y--) res *= x;
    return res;
}


LongInt::LongInt(string a) {

    reverse(a.begin(), a.end());
    for (int i = 0; i < a.length(); i += Base) {
        int b;
        int x = 0;
        for (int j = i; j < min(i + Base, int(a.length())); j++) {
            b = int((a[j] - '0') * Pow(10, ((j - i))));
            x += b;
        }
        digits.push_back(x);
    }
    if (len() % 2 != 0) {
        digits.push_back(0);
    }
}

LongInt::LongInt(int a) {
    while (a >= 1) {
        int b;
        int x = 0;
        for (int j = 0; j < Base; j++) {
            b = int((a % 10) * Pow(10, j));
            x += b;
            a /= 10;
        }
        digits.push_back(x);
    }
    if (len() % 2 != 0) {
        digits.push_back(0);
    }
    if (digits.empty()){
        digits.push_back(0);
    }
}

int LongInt::len() {
    int n = digits.size();
    return n;
}


void LongInt::shift(int a) {
    reverse(digits.begin(), digits.end());
    for (int i = 0; i < a ; i++) {
        this->digits.push_back(0);
    }
    reverse(digits.begin(), digits.end());
}
LongInt LongInt::split(int a) {
    LongInt w=*this;

    if (a == 2) {
        for (int i = 0; i < (len() / 2); i++) {
            w.digits.pop_back();
        }
    } else if (a == 1) {
        reverse(w.digits.begin(), w.digits.end());
        for (int i = (len() / 2); i < len(); i++) {

            w.digits.pop_back();
        }
        reverse(w.digits.begin(), w.digits.end());
    }
    if(w.len()==0){
        LongInt r=LongInt(0);
        return r;
    }
    else{
        return w;
    }
}

void LongInt::setMultMetod(Multiplication *mult) {
    multiplication=mult;
}

void LongInt::setPrimeMethod(PrimeCheck *prc) {
    Check=prc;
}

void LongInt::equalize(LongInt &x, LongInt &y) {
    while (x.len() > y.len()) {
        y.digits.push_back(0);
    }
    while (y.len() > x.len()) {
        x.digits.push_back(0);
    }
}

string LongInt::ConvToStr() {

    reverse(digits.begin(), digits.end());
    string a;

    for (int i = 0; i < len(); i++) {
        int x = digits[i];
        vector<char> VEC;
        for (int j = 0; j < Base; j++) {
            VEC.push_back(x % 10 + '0');
            x /= 10;
            if (x == 0 && i == 0) {
                break;
            }
        }
        reverse(VEC.begin(), VEC.end());
        for (int i:VEC) a.push_back(i);
    }
    reverse(a.begin(), a.end());
    while (a.size() > 1 && a.back() == '0') a.pop_back();
    reverse(a.begin(), a.end());
    return a;
}

LongInt LongInt::operator+(LongInt other) {
    equalize(*this, other);
    LongInt res(0);
    res.digits = vector<int>(len() + 1, 0);
    for (int i = 0; i < len(); i++) {
        int x=res.digits[i]+(digits[i] + other.digits[i]);
        res.digits[i] = x%BASE;
        res.digits[i + 1] = (x) / BASE;
    }
    res.RemoveZeros();
    RemoveZeros();
    other.RemoveZeros();
    return res;
}

LongInt LongInt::operator*(int other){
    LongInt res(0);
    res.digits.resize(len()+1);
    int add=0;
    for (int i=0; i<len(); ++i){
        add+=digits[i]*other;
        res.digits[i]=add%BASE;
        add/=BASE;
    }
    res.digits.back()=add;
    return res;
}


LongInt LongInt::operator*(LongInt other) {

    return multiplication->multiply(*this, other);

}

bool LongInt::Is_Prime() {

    return Check->primecheck(*this);

}


LongInt LongInt::operator-(LongInt other) {
    equalize(*this, other);
    LongInt res=LongInt(0);
    res.digits = vector<int>(len(), 0);
    for (int i = 0; i < len(); i++) {
        if ((digits[i] - other.digits[i] + res.digits[i]) >= 0) {
            res.digits[i] += (digits[i] - other.digits[i]);
        } else {
            res.digits[i + 1]--;
            res.digits[i] += (digits[i] + BASE - other.digits[i]);
        }

    }
    return res;
}

bool operator<(LongInt x, LongInt y) {
    x.RemoveZeros();
    y.RemoveZeros();
    if (x.len()!=y.len()) return x.len()<y.len();
    for (int i=x.len()-1; i>=0; --i){
        if (x.digits[i]!=y.digits[i]) return x.digits[i]<y.digits[i];
    }
    return 0;
}

bool operator<=(LongInt x, LongInt y) {
    x.RemoveZeros();
    y.RemoveZeros();
    return x.digits==y.digits || x<y;
}

LongInt LongInt::operator/(LongInt other) {
    auto x=(*this);
    auto z=other;
    vector <LongInt> VECt;

    while (z<=(*this)){
        VECt.push_back(z);
        z.shift(1);
    }
    LongInt res(0);
    vector <int> VEC;
    reverse(VECt.begin(),VECt.end());

    for (auto y:VECt){
        VEC.push_back(0);
        int l=0;
        int r=BASE;
        while (l+1<r){
            int m=(l+r)/2;
            if ((LongInt(m)*y)<=x) l=m;
            else r=m;
        }
        VEC.back()=l;

        auto z=LongInt(VEC.back())*y;
        x=x-z;
    }
    reverse(VEC.begin(),VEC.end());
    res=LongInt(0);
    res.digits=VEC;
    if (VEC.size()==0) return LongInt(0);
    return res;
}

LongInt LongInt::operator%(LongInt other) {
    auto z=(*this)/other*other;
    return (*this)-z;
}

void LongInt::RemoveZeros() {
    while (digits.size() > 1 && digits.back() == 0) digits.pop_back();
}

ostream &operator<<(ostream &os, LongInt x) {
    os << x.ConvToStr();
    return os;
}

bool operator == (LongInt a, LongInt b){

    return !((a<b) || (b<a));
}

LongInt ModPow(LongInt a, LongInt p, LongInt mod){
    auto zero = LongInt(0);
    if (p==zero) return LongInt(1);
    if (p==LongInt(1)) return a;
    auto res=ModPow(a,p/LongInt(2),mod);
    res=res*res;
    res=res%mod;
    auto x=ModPow(a,p%LongInt(2),mod);
    res=res*x;
    res=res%mod;
    return res;
}

std::string Conclusion(bool a){
    if (a) {
        cout << " is prime";
    }
    else cout << " is not prime";
}
