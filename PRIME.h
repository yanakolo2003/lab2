//
// Created by Яна on 01.11.2022.
//

#include "LONG.h"
#ifndef LAB2_FERMAPRIME_H

class FERMA: public PrimeCheck {
public:
    bool primecheck(LongInt a);
};

class Solovay_Strassen: public PrimeCheck {
public:
    bool primecheck(LongInt a);
};

#define LAB2_FERMAPRIME_H

#endif //LAB2_FERMAPRIME_H

